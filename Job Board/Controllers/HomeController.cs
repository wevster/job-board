﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Job_Board.Controllers
{
    public class HomeController : Controller
    {
        jobEntities db = new jobEntities();
        public ActionResult Index()
        {
            return View(db.Jobs.ToList());
        }

        public ActionResult Add() {


            return View();
        }


        [HttpPost]
        public ActionResult Add(Job job)
        {
            job.CreatedAt = DateTime.Now;
            db.Jobs.Add(job);
            db.SaveChanges();            
            ModelState.Clear();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult update(int? id)
        {
            return View(db.Jobs.Find(id));
        }

        [HttpPost]
        public ActionResult update(Job job)
        {
            db.Entry(job).State = System.Data.Entity.EntityState.Modified;
            db.SaveChanges();
            ModelState.Clear();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult delete(int? id)
        {
            Job job = db.Jobs.Find(id);
            db.Jobs.Remove(job);
            db.SaveChanges();
            return RedirectToAction("Index", "Home");
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}